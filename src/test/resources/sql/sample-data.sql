insert into topik (id, code, name, harga) 
values ('j001', 'J-001', 'Java Fundamental', 1200000.00);

insert into peserta (id, email, nama, no_hp)
values ('p001', 'p001@yopmail.com', 'Peserta 001', '08123456789001');

insert into peserta (id, email, nama, no_hp)
values ('p002', 'p002@yopmail.com', 'Peserta 002', '08123456789002');

insert into peserta (id, email, nama, no_hp)
values ('p003', 'p003@yopmail.com', 'Peserta 003', '08123456789003');

insert into instruktur (id, email, nama, no_hp)
values ('i001', 'i001@yopmail.com', 'Instruktur 001', '08123456789101');

insert into jadwal (id, id_topik, tanggal_mulai, tanggal_selesai) 
values ('jd001', 'j001', '2021-02-01', '2021-02-05');

insert into materi (id, id_topik, kode, nama, durasi_menit)
values ('m001', 'j001', 'J-001-01', 'Instalasi JDK', 30);

insert into materi (id, id_topik, kode, nama, durasi_menit)
values ('m002', 'j001', 'J-001-02', 'Hello World', 45);

insert into materi (id, id_topik, kode, nama, durasi_menit)
values ('m003', 'j001', 'J-001-03', 'Package dan Jar', 60);

insert into batch (id, id_jadwal, tanggal_mulai, tanggal_selesai)
values ('b001', 'jd001', '2021-03-01', '2021-03-04');

insert into daftar_tanggal_batch (id_batch, tanggal_pelatihan)
values ('b001', '2021-03-01');

insert into daftar_tanggal_batch (id_batch, tanggal_pelatihan)
values ('b001', '2021-03-02');

insert into daftar_tanggal_batch (id_batch, tanggal_pelatihan)
values ('b001', '2021-03-03');

insert into daftar_tanggal_batch (id_batch, tanggal_pelatihan)
values ('b001', '2021-03-04');

insert into daftar_materi_batch (id_batch, id_materi)
values ('b001', 'm001');

insert into daftar_materi_batch (id_batch, id_materi)
values ('b001', 'm002');

insert into sesi (id, id_batch, id_instruktur, tanggal, jam_mulai, jam_selesai)
values ('sesi01', 'b001', 'i001', '2021-03-01', '08:00:01', '16:15:00');

insert into sesi (id, id_batch, id_instruktur, tanggal, jam_mulai, jam_selesai)
values ('sesi02', 'b001', 'i001', '2021-03-03', '08:30:01', '16:00:00');

insert into presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
values ('pr101', 'sesi01', 'p001', '08:05:00', '16:15:00');

insert into presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
values ('pr102', 'sesi01', 'p002', '08:00:00', '16:15:00');

insert into presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
values ('pr201', 'sesi02', 'p003', '09:05:00', '15:15:00');

insert into presensi (id, id_sesi, id_peserta, jam_masuk, jam_keluar)
values ('pr202', 'sesi02', 'p001', '08:15:00', '16:00:00');