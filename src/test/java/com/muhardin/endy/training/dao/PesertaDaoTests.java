package com.muhardin.endy.training.dao;

import java.util.List;

import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.entity.Presensi;
import com.muhardin.endy.training.entity.Topik;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {
    "classpath:/sql/reset-data.sql",
    "classpath:/sql/sample-data.sql"
})
public class PesertaDaoTests {
    
    @Autowired private PresensiDao presensiDao;

    @Test
    public void testCariPesertaYangTelahIkutTopikTertentu(){
        Topik t = new Topik();
        t.setId("j001");

        List<Presensi> daftarPresensiSemuaTrainingJavaFundamental
            = presensiDao.findBySesiBatchJadwalTopik(t);

        Assertions.assertEquals(4, daftarPresensiSemuaTrainingJavaFundamental.size());
        
        System.out.println("=============================");

        List<Peserta> daftarPesertaJavaFundamental
            = presensiDao.findPesertaForTopik(t);

        Assertions.assertEquals(3, daftarPesertaJavaFundamental.size());

        for(Peserta p : daftarPesertaJavaFundamental){
            System.out.println("Nama peserta : "+p.getNama());
        }

        List<String> daftarEmailPesertaTrainingJavaFundameString
            = presensiDao.findEmailPesertaForTopik(t);

        Assertions.assertEquals(3, daftarEmailPesertaTrainingJavaFundameString.size());

        for(String email : daftarEmailPesertaTrainingJavaFundameString) {
            System.out.println("Email : "+email);
        }
    }
}
