package com.muhardin.endy.training.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

import javax.sql.DataSource;

import com.muhardin.endy.training.entity.Jadwal;
import com.muhardin.endy.training.entity.Topik;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/reset-data.sql",
        "classpath:/sql/sample-data.sql"
    }
)
public class JadwalDaoTests {
    private static final String SQL_HITUNG_JADWAL = "select count(*) from jadwal";
    
    @Autowired TopikDao topikDao;
    @Autowired private JadwalDao jadwalDao;

    @Autowired private DataSource dataSource;


    @Test
    public void testSaveTopik(){
        Topik t = new Topik();
        t.setCode("W-001");
        t.setName("Web Programming Basic");
        t.setHarga(new BigDecimal("1100000.00"));

        topikDao.save(t);

        Assertions.assertNotNull(t.getId());
    }

    @Test
    public void testSaveJadwal() throws SQLException {
        Jadwal j = new Jadwal();
        j.setTanggalMulai(LocalDate.of(2021, 01, 01));
        j.setTanggalSelesai(LocalDate.of(2021, 01, 05));

        Optional<Topik> t = topikDao.findById("j001"); 

        Assertions.assertTrue(t.isPresent());
        Assertions.assertEquals("Java Fundamental", t.get().getName());

        j.setTopik(t.get());

        ResultSet rsSebelum = dataSource.getConnection().createStatement().executeQuery(SQL_HITUNG_JADWAL);
        Assertions.assertTrue(rsSebelum.next());
        Long jumlahSebelumSave = rsSebelum.getLong(1);
        
        Assertions.assertNull(j.getId());
        jadwalDao.save(j);
        Assertions.assertNotNull(j.getId());

        ResultSet rsSetelah = dataSource.getConnection().createStatement().executeQuery(SQL_HITUNG_JADWAL);
        Assertions.assertTrue(rsSetelah.next());
        Long jumlahSetelahSave = rsSetelah.getLong(1);
        
        Assertions.assertTrue(jumlahSetelahSave == jumlahSebelumSave + 1);
    }

}
