package com.muhardin.endy.training.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired private DataSource dataSource;

    private static final String SQL_LOGIN
            = "select u.username as username,p.password as password, active "
            + "from s_user u "
            + "inner join s_user_password p on p.id_user = u.id "
            + "where username = ?";

    private static final String SQL_ROLE
            = "select u.username, p.permission_value as authority "
            + "from s_user u "
            + "inner join s_role r on u.id_role = r.id "
            + "inner join s_role_permission rp on rp.id_role = r.id "
            + "inner join s_permission p on rp.id_permission = p.id "
            + "where u.username = ?";

    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(SQL_LOGIN)
                .authoritiesByUsernameQuery(SQL_ROLE);
    }

    @Configuration @Order(1)
    static class KonfigurasiSecurityApi extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/api/**")
            .authorizeRequests((authorize -> authorize
                .anyRequest().authenticated()
            ))
            .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);
        }
    }
    
    @Configuration @Order(2)
    static class KonfigurasiSecurityHtml extends WebSecurityConfigurerAdapter {

        private static final String SQL_PERMISSION_ONLY
            = "select p.permission_value as authority "
            + "from s_user u "
            + "inner join s_role r on u.id_role = r.id "
            + "inner join s_role_permission rp on rp.id_role = r.id "
            + "inner join s_permission p on rp.id_permission = p.id "
            + "where u.username = ?";

        @Autowired private ObjectMapper objectMapper;
        @Autowired private JdbcTemplate jdbcTemplate;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
            .authorizeRequests()
            .antMatchers("/login").permitAll()
            .anyRequest().authenticated()
            .and().formLogin().loginPage("/login")
            .and().oauth2Login().loginPage("/login")
            .userInfoEndpoint().userAuthoritiesMapper(mappingPermissionUserSso());
            //.and().sessionManagement().maximumSessions(1)
            //.maxSessionsPreventsLogin(false);
            // true : yang login duluan menang, login belakangan error
            // false : yang login belakangan menang, login duluan dipaksa logout
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring()
                    .antMatchers("/js/**")
                    .antMatchers("/img/**")
                    .antMatchers("/css/**");
        }

        private GrantedAuthoritiesMapper mappingPermissionUserSso(){
            return (authorities) -> {
                List<GrantedAuthority> daftarPermission 
                    = new ArrayList<>();

                // konversi username dari google/sso menjadi permission
                try {
                    log.debug("Daftar authority dari google : {}",
                    objectMapper.writeValueAsString(authorities));

                    // ambil emailnya saja
                    String emailAttrName = "email";
                    String email = authorities.stream()
                        .filter(OAuth2UserAuthority.class::isInstance)
                        .map(OAuth2UserAuthority.class::cast)
                        .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                        .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                        .findFirst()
                        .orElse(null);

                    log.debug("Email : {}", email);
                    
                    if(email == null){
                        return authorities;
                    }

                    // query ke database, cari permission by email
                    List<String> permissionListDb = jdbcTemplate.queryForList(SQL_PERMISSION_ONLY, String.class, email);
                    log.debug("Daftar permission dari database : {}", permissionListDb);

                    for(String permission : permissionListDb) {
                        daftarPermission.add(new SimpleGrantedAuthority(permission));
                    }

                } catch (JsonProcessingException e) {
                    log.error(e.getMessage(), e);
                }

                return daftarPermission;
            };
        }
    }

}
