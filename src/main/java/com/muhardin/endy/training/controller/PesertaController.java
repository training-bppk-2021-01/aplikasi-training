package com.muhardin.endy.training.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.muhardin.endy.training.dao.PesertaDao;
import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.service.FileUploadService;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;

@Slf4j
@Controller
@RequestMapping("/peserta")
public class PesertaController {

    @Autowired private PesertaDao pesertaDao;
    @Autowired private FileUploadService fileUploadService;
    
    @Value("classpath:jasperreports/peserta.jrxml")
    private Resource templateJasperReport;

    private JasperReport reportPeserta;

    @GetMapping("/report")
    public ResponseEntity<byte[]> generateReport() throws JRException, IOException{
        // load dan compile template
        if(reportPeserta == null){
            reportPeserta = JasperCompileManager.compileReport(templateJasperReport.getInputStream());
        }

        // isi report dengan data
        Map<String, Object> params = new HashMap<>();
        params.put("tanggalCetak", LocalDateTime.now().toString());
        Iterable<Peserta> daftarPeserta = pesertaDao.findAll();
        
        JasperPrint jrPrint = JasperFillManager
                .fillReport(reportPeserta, params, new JRBeanCollectionDataSource(
                    IteratorUtils.toList(daftarPeserta.iterator())));

        // render output ke PDF
        byte[] reportPdf = JasperExportManager.exportReportToPdf(jrPrint);
        return ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_PDF)
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"peserta.pdf\"")
        .body(reportPdf);
    }

    @GetMapping("/list")
    public ModelMap dataPeserta(Pageable page, Authentication currentUser){
        ModelMap mm = new ModelMap();
        mm.addAttribute("daftarPeserta", pesertaDao.findAll(page));

        mm.addAttribute("daftarPermission", currentUser.getAuthorities());
        log.debug("User yang sedang login : "+currentUser.getName());
        log.debug("Daftar Permission : "+currentUser.getAuthorities());

        return mm;
    }

    @PreAuthorize(value = "hasAuthority('EDIT_TAGIHAN')")
    @GetMapping("/form")
    public void displayInputForm(){

    }

    @PreAuthorize(value = "hasAuthority('EDIT_TAGIHAN')")
    @PostMapping("/form")
    public String prosesForm(@ModelAttribute @Valid Peserta peserta, BindingResult errors, 
                            @RequestParam("fileFoto") MultipartFile fileFoto, SessionStatus status) throws IOException{
        log.debug("Memproses form input peserta");

        if(errors.hasErrors()) {
            log.debug(errors.toString());
            return "peserta/form";
        }

        String extension = FileUploadService.getExtensionByStringHandling(fileFoto.getOriginalFilename())
        .orElse("jpg");

        String namafile = "foto-" + UUID.randomUUID().toString() + "." + extension;
        
        fileUploadService.simpanFile(fileFoto.getInputStream(), namafile);
        peserta.setFoto(namafile);
        pesertaDao.save(peserta);

        return "redirect:form";
    }
}
