package com.muhardin.endy.training.controller.api;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.muhardin.endy.training.dao.PesertaDao;
import com.muhardin.endy.training.entity.Peserta;
import com.muhardin.endy.training.service.FileUploadService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/api/peserta")
public class PesertaApiController {

    @Autowired private PesertaDao pesertaDao;
    @Autowired private FileUploadService fileUploadService;

    @GetMapping("/")
    @ResponseBody
    public Page<Peserta> dataPeserta(Pageable page){
        // argumen Pageable akan memproses parameter di request
        // contoh : ?size=2&page=0&sort=email,desc
        return pesertaDao.findAll(page);
    }

    @PostMapping("/")
    public ResponseEntity<Void> simpan(@RequestBody @Valid Peserta peserta, 
                UriComponentsBuilder uriBuilder){
        pesertaDao.save(peserta);
        UriComponents uriComponents = uriBuilder
                .path("/api/peserta/{id}")
                .buildAndExpand(peserta.getId());

        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @PutMapping("/{id}/foto") @ResponseStatus(HttpStatus.OK)
    public void uploadFoto(@PathVariable("id") Peserta peserta, 
                @RequestParam("foto") MultipartFile fileFoto) throws IOException{
        log.debug("Nama File : "+fileFoto.getOriginalFilename());
        String extension = FileUploadService.getExtensionByStringHandling(fileFoto.getOriginalFilename())
        .orElse("jpg");

        String namaFile = fileUploadService.simpanFile(fileFoto.getInputStream(), 
        "foto-" + peserta.getId() + "." + extension);

        peserta.setFoto(namaFile);
        pesertaDao.save(peserta);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Peserta> pesertaById(@PathVariable("id") String id) {
        Optional<Peserta> optPeserta =  pesertaDao.findById(id);
        if(optPeserta.isPresent()) {
            return ResponseEntity.ok()
            .body(optPeserta.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}/foto")
    @ResponseBody
    public ResponseEntity<Resource> fotoPeserta(@PathVariable("id") Peserta peserta, HttpServletRequest request){
        String namafileFoto = peserta.getFoto();
        Resource fileFoto = fileUploadService.loadFileAsResource(namafileFoto);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(fileFoto.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.warn("Mime Type file {} tidak diketahui", namafileFoto);
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        // Header Content-Disposition bisa diisi inline supaya memunculkan opsi download di browser
        //.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileFoto.getFilename() + "\"")
        .body(fileFoto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(
            @PathVariable(value = "id", required = false) Peserta pesertaDb,
            @RequestBody @Valid Peserta pesertaInput){
        if(pesertaDb == null) {
            return ResponseEntity.notFound().build();
        } 

        pesertaInput.setId(pesertaDb.getId());
        BeanUtils.copyProperties(pesertaInput, pesertaDb);
        pesertaDao.save(pesertaDb);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(value = "id", required = false) Peserta peserta){
        if(peserta != null) {
            pesertaDao.delete(peserta);
        }
    }

    
}
