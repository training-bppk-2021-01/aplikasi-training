package com.muhardin.endy.training.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data 
public class Instruktur {
    @Id @Column(length = 36)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Email @Size(max = 100)
    private String email;

    @NotNull @NotEmpty @Size(max = 255)
    private String nama;

    @NotNull @NotEmpty @Size(max = 50)
    private String noHp;
}
