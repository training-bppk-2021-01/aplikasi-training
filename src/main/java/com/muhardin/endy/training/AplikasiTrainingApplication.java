package com.muhardin.endy.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;

@SpringBootApplication
public class AplikasiTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiTrainingApplication.class, args);
	}

	@Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
}
