create table topik (
    id varchar(36),
    kode varchar(50) not null,
    nama varchar(255) not null,
    durasi_jam int not null,
    primary key (id),
    unique key (kode)
);