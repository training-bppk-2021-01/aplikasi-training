# Aplikasi Pengelolaan Training #

## Menjalankan Database Development ##

```
docker run --name dbtraining \
           -e MYSQL_RANDOM_ROOT_PASSWORD=true \
           -e MYSQL_USER=training \
           -e MYSQL_PASSWORD=training123 \
           -e MYSQL_DATABASE=trainingdb \
           -v "$PWD/trainingdb-data:/var/lib/mysql" \
           -p 3306:3306 --rm mariadb:10
```

## Mendapatkan Access Token dari Google ##

1. Buka URL authorization dengan browser

    ```
    https://accounts.google.com/o/oauth2/v2/auth?response_type=code&client_id=820278119126-bjam88s64d9ab037jrd2j0okrkqfki5b.apps.googleusercontent.com&scope=openid%20email&redirect_uri=http%3A//example.com&state=1234
    ```

2. Mendapatkan authorization code dari URL redirect

    ```
    http://example.com/?state=1234&code=4%2F0AX4XfWj7LEA1HQGUEJbC-EdVTuZnG8FNY7utBMe24WBI-IfC1MPZMjB5aohv6rntbXHk1g&scope=email+openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&authuser=0&hd=artivisi.com&prompt=none#
    ```

    Code : `4%2F0AX4XfWj7LEA1HQGUEJbC-EdVTuZnG8FNY7utBMe24WBI-IfC1MPZMjB5aohv6rntbXHk1g`

3. Mengakses URL token untuk menukarkan authorization code menjadi access token

    * POST ke `https://oauth2.googleapis.com/token`
    * Parameter Request : 

        * code : `4%2F0AX4XfWj7LEA1HQGUEJbC-EdVTuZnG8FNY7utBMe24WBI-IfC1MPZMjB5aohv6rntbXHk1g`
        * client_id : `820278119126-bjam88s64d9ab037jrd2j0okrkqfki5b.apps.googleusercontent.com`
        * client_secret : `m-UWYqqWJ2L02OH0fDrWdWWf`
        * redirect_uri : `http://example.com`
        * grant_type : `authorization_code`
        

4. Mendapatkan access token dan id token dari Google

    ```
    {
        "access_token": "ya29.a0ARrdaM9Ig8ynFVNPfHRXKSfqpo8COP0khcy8SuZF9-ynD7TIlWcDSmSdvJNXZBKb8hiv5lapZ0W1zcECO-9OOIg6oZqgIkDxH_Bwd415nbZDg-tOZfMAr-2klavQRCNTo_yXyvynmY2R7OD2YN2zHg8IFb548oRtZ9Q",
        "expires_in": 3304,
        "scope": "openid https://www.googleapis.com/auth/userinfo.email",
        "token_type": "Bearer",
        "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjhkOTI5YzYzZmYxMDgyYmJiOGM5OWY5OTRmYTNmZjRhZGFkYTJkMTEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI4MjAyNzgxMTkxMjYtYmphbTg4czY0ZDlhYjAzN2pyZDJqMG9rcmtxZmtpNWIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI4MjAyNzgxMTkxMjYtYmphbTg4czY0ZDlhYjAzN2pyZDJqMG9rcmtxZmtpNWIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTcxNzYwMTQ5NjI2MjYwMjAzNTkiLCJoZCI6ImFydGl2aXNpLmNvbSIsImVtYWlsIjoiZW5keUBhcnRpdmlzaS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6Im5wdkVfbzc4Nkc3SW1ubXJpOUlGZ3ciLCJpYXQiOjE2MzMwNTc3NDksImV4cCI6MTYzMzA2MTM0OX0.4XyoKQRUHr8u4Olbihpc4P227HekA3qKscjQjnTd4yyX6lolqNM_T6LCwW2GsKrbWhQqs40V4lkQPGTfp0YH5k4i_B0WPWPkJN0EdpDMthkvWjX9ja4MZrDeTp82vQ6bvFt0J3xaTpgUkdjTYWsY7qoD8Nh3cwGBBV_nuhyA-MoM5cNjNYy4DWKmyXKO1wA0_vzhS_ELaJcBLCgIjUyYZulqEJMao1icLNPHv5MPA4io10qcqYtEdeVTsXhHXOVTb1GZXYj-cz9mYwvuonPe209SBS0zw7Y7z5_jHOV9uNZx5tN54FPSv8I9dkNbBT2Dnh1EE_o3e7imWybNARDwIw"
    }
    ```

5. Gunakan id token untuk mengakses API `http://localhost:8080/api/materi/list`